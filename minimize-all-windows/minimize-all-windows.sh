#!/bin/sh

lock=/tmp/wmctrl.lock
minimized=/tmp/wmctrl.is-minimized

for i in lockfile wmctrl; do
	if ! which $i; then
		xmessage -nearmouse "No \"$i\" program. Please, install it."
		exit
	fi
done

lockfile "$lock" || exit

if [ -f "$minimized" ]; then
	rm -f "$minimized"
	wmctrl -k off
else
	touch "$minimized"
	wmctrl -k on
fi

rm -f "$lock"
