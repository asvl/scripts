#!/usr/bin/tclsh
#
# Output all existing vars and arrays

foreach i [info vars] {
	if ![array exists $i] {
		puts -nonewline "$$i var: "
		eval puts "$$i"
		puts ""
	} else {
		puts -nonewline "$$i array: "
		puts [array get "$i"]
		puts ""
	}
}
