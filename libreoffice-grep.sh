#!/bin/sh

which odt2txt > /dev/null || { echo "odt2txt is not installed?"; exit 1; }
[ "$1" ] || { echo "You forgot search string!"; exit 1; }

find . -type f -iname "*.od*" | while read i; do
	str=`odt2txt "$i" | grep "$*"`
	if [ $? -eq 0 ] ; then
		echo; echo "Found in $i"; echo "$str"
	fi
done
