#!/usr/bin/python

# Test bits (specified in command args) again digits from stdin.

from __future__ import print_function		# for file= in print()
import sys


# Look for bits in args
bits = []
i = 0
for a in sys.argv[1:]:
	try:
		n = int(a)
	except:
		try:
			n = int(a, 16)
		except:
			print(a + ' can\'t convert bits to digit',
					file = sys.stderr)
			continue
	bits.append(n)

# Bits header
print('\n\tBits ', end = '')
for i in bits:
	print('.' + '{0:^3}'.format(str(i)), end = '')
print('\n')

for l in sys.stdin:
	l = str.strip(l)

	try:
		n = int(l)
	except:
		try:
			n = int(l, 16)
		except:
			print(l + ' can\'t convert to digit',
					file = sys.stderr)
			continue

	print(l + '\t', end = '')
	for bit in bits:
		if (n & (1 << bit)):
			print(' . 1', end = '')
		else:
			print(' . 0', end = '')
	print('')
