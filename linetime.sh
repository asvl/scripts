#!/bin/bash
#
# Print time on every line, if time is differ
#
# Test:
#	( echo a; sleep 2; echo b ) | ./linetime.sh

ta=`date +%s`
echo $ta
while read -n $((3*7)) l; do
	tb=`date +%s`
	if [ $tb = $ta ]; then
		echo -e "\t$l"
	else
		echo -e "`date`\t$l"
		ta=$tb
	fi
done
date +%s
