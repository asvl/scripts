for i in djvups ps2pdf14; do
	if ! which $i > /dev/null; then
		echo "Not found: $i"
		exit 1
	fi
done

find . -name '*.djvu' -print | while read f; do
	echo $f
	[ -f "$f".pdf ] && continue
	djvups "$f" "$f".ps; ps2pdf14 "$f".ps; rm "$f".ps
done
