#!/bin/sh

#		-c "set formatoptions=a" \
gvim -geom -400+300 \
		-c "set spell spelllang=ru_yo,en" \
		-c "set nocindent" \
		-c "set textwidth=76" \
		$*
