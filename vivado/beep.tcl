#!/usr/bin/tclsh

if [info exist num_errors] {
	if {$num_errors == 0} {
		exec -- beep -r 3
	} else {
		exec -- beep -r 5
	}
} else {
	exec -- beep -r 3
}
