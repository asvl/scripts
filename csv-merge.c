#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	FILE *a, *b;
	const int line_len = 64*1024;
	char ba[line_len], bb[line_len], *ra, *rb;

	if (argc < 3) {
		printf("Usage: %s file1.csv file2.csv > output.csv\n", argv[0]);
		return 1;
	}

	a = fopen(argv[1], "r");
	if (a == NULL) {
		printf("Can't open file %s\n", argv[1]);
		return 2;
	}

	b = fopen(argv[2], "r");
	if (b == NULL) {
		printf("Can't open file %s\n", argv[2]);
		fclose(a);
		return 3;
	}

	while (1) {
		ra = fgets(ba, line_len, a);
		rb = fgets(bb, line_len, b);

		if (ra == NULL && rb == NULL)
			break;

		if (ra != NULL) {
			ba[strlen(ba) - 1] = '\0';
			printf(ba);
		}

		if (ra != NULL && rb != NULL)
			printf(", ");
		else
			fputs("Files with NOT equal line counts\n", stderr);

		if (rb != NULL) {
			bb[strlen(bb) - 1] = '\0';
			printf(bb);
		}

		puts("");
	}

	fclose(a);
	fclose(b);

	return 0;
}
