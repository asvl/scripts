#!/usr/bin/tclsh

foreach i $argv {
	set i_low [string tolower "$i"]
	if {"$i_low" != "$i"} {
		file rename "$i" "$i_low"
	}
}

