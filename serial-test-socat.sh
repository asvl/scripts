#!/bin/sh

baud=115200
data_file=rnd1m
params=sane,raw,icanon=0,echo=0,nonblock,ignoreeof,flusho=1,crtscts=1

port_a=$1
port_b=$2

if [ x$BAUD != x ]; then
	baud=$BAUD
fi

if [ x$1 = x -o x$1 = x ]; then
	echo "Usage: $0 port_a port_b"

	exit 1
fi

file_a=/tmp/rx-$port_a
file_b=/tmp/rx-$port_b

# NOTE: if block_size is not written at once, then socat will just drop
#       remaining data in current block.
block_size=2048

md5() {
	md5sum $1 | cut -d' ' -f1-1
}

socat -b$block_size -d -T3 stdio \
	/dev/$port_a,b$baud,$params \
	< $data_file > $file_a &

socat -b$block_size -d -T3 stdio \
	/dev/$port_b,b$baud,$params \
	< $data_file > $file_b

sleep 2

data_file_md5=`md5 $data_file`

for i in $file_a $file_b; do
	if [ $data_file_md5 != `md5 $i` ]; then
		echo ERROR: md5sum of $i
	else
		echo OK for $i

	fi
done
